
# coding: utf-8

# In[ ]:


import pandas as pd
import glob,os
from datetime import datetime


# In[ ]:


path = raw_input('Enter path to where count records are stored')
os.chdir(path)
records = glob.glob("*.xls")
df_out = pd.DataFrame()


# In[ ]:


for record in records:
    try:
        print record
        df1= pd.read_excel(record,skiprows=11)
        df2 = pd.read_excel(record, nrows=6,names=['CODE','Empty1','Empty2','Empty3'])
        df1['stationno'] = df2['CODE'][2].split(' ')[2]
        df1['Location'] = ' '.join(df2['CODE'][5].split(' ')[2:])
        df1['Road'] = ' '.join(df2['CODE'][4].split(' ')[3:])
        df_out = df_out.append(df1,ignore_index=True)
    except:
        print "Nope"+record


# In[ ]:


df2


# In[ ]:


df1


# In[ ]:


df_out


# In[ ]:


df_out['Date'] = pd.to_datetime(df_out['Date'])
df_out['Date_1'] = pd.to_datetime(df_out['Date'])
df_out['Time'] = pd.to_datetime(df_out['Time']).dt.strftime('%I:%M:%S %p')


# In[ ]:


df_out['Mon'] = df_out['Date'].dt.strftime('%b')
df_out['Year'] = df_out['Date'].dt.year
df_out['Day'] = pd.to_datetime(df_out['Date']).dt.strftime('%a')
df_out['Date'] = df_out['Date'].dt.strftime('%m/%d/%Y')
# df_out['Date'] = df_out['Date'].dt.strftime('%B') + " " + df_out['Date'].dt.strftime('%Y')
df_out['County'] = 'Canyon'
df_out['Agency'] = 'NHD'
df_out['Interval'] = '15'
df_out['Counttype'] = 'Portable'
df_out['Onetwoway'] = '2'


# In[ ]:


direction_1 = df_out.pivot_table(index=['Date','Date_1','stationno','Location','Road','Mon','Year','Day','County','Agency','Interval','Counttype','Onetwoway'],columns='Time',values='Direction 1',aggfunc='sum',margins=True,margins_name='Total').reset_index()
direction_1['Direction'] = 'North or West'
direction_1['otherdirection'] = '1'
direction_2 = df_out.pivot_table(index=['Date','Date_1','stationno','Location','Road','Mon','Year','Day','County','Agency','Interval','Counttype','Onetwoway'],columns='Time',values='Direction 2',aggfunc='sum',margins=True,margins_name='Total').reset_index()
direction_2['Direction'] = 'South or East'
direction_2['otherdirection'] = '2'
concat = pd.concat([direction_1,direction_2])


# In[ ]:


concat.columns.values
df_rename = concat.rename(columns={'01:00:00 AM':'am1','01:00:00 PM':'pm1','01:15:00 AM':'am1_15','01:15:00 PM':'pm1_15','01:30:00 AM':'am1_30','01:30:00 PM':'pm1_30','01:45:00 AM':'am1_45','01:45:00 PM':'pm1_45','02:00:00 AM':'am2','02:00:00 PM':'pm2','02:15:00 AM':'am2_15','02:15:00 PM':'pm2_15','02:30:00 AM':'am2_30','02:30:00 PM':'pm2_30','02:45:00 AM':'am2_45','02:45:00 PM':'pm2_45','03:00:00 AM':'am3','03:00:00 PM':'pm3','03:15:00 AM':'am3_15','03:15:00 PM':'pm3_15','03:30:00 AM':'am3_30','03:30:00 PM':'pm3_30','03:45:00 AM':'am3_45','03:45:00 PM':'pm3_45','04:00:00 AM':'am4','04:00:00 PM':'pm4','04:15:00 AM':'am4_15','04:15:00 PM':'pm4_15','04:30:00 AM':'am4_30','04:30:00 PM':'pm4_30','04:45:00 AM':'am4_45','04:45:00 PM':'pm4_45','05:00:00 AM':'am5','05:00:00 PM':'pm5','05:15:00 AM':'am5_15','05:15:00 PM':'pm5_15','05:30:00 AM':'am5_30','05:30:00 PM':'pm5_30','05:45:00 AM':'am5_45','05:45:00 PM':'pm5_45','06:00:00 AM':'am6','06:00:00 PM':'pm6','06:15:00 AM':'am6_15','06:15:00 PM':'pm6_15','06:30:00 AM':'am6_30','06:30:00 PM':'pm6_30','06:45:00 AM':'am6_45','06:45:00 PM':'pm6_45','07:00:00 AM':'am7','07:00:00 PM':'pm7','07:15:00 AM':'am7_15','07:15:00 PM':'pm7_15','07:30:00 AM':'am7_30','07:30:00 PM':'pm7_30','07:45:00 AM':'am7_45','07:45:00 PM':'pm7_45','08:00:00 AM':'am8','08:00:00 PM':'pm8','08:15:00 AM':'am8_15','08:15:00 PM':'pm8_15','08:30:00 AM':'am8_30','08:30:00 PM':'pm8_30','08:45:00 AM':'am8_45','08:45:00 PM':'pm8_45','09:00:00 AM':'am9','09:00:00 PM':'pm9','09:15:00 AM':'am9_15','09:15:00 PM':'pm9_15','09:30:00 AM':'am9_30','09:30:00 PM':'pm9_30','09:45:00 AM':'am9_45','09:45:00 PM':'pm9_45','10:00:00 AM':'am10','10:00:00 PM':'pm10','10:15:00 AM':'am10_15','10:15:00 PM':'pm10_15','10:30:00 AM':'am10_30','10:30:00 PM':'pm10_30','10:45:00 AM':'am10_45','10:45:00 PM':'pm10_45','11:00:00 AM':'am11','11:00:00 PM':'pm11','11:15:00 AM':'am11_15','11:15:00 PM':'pm11_15','11:30:00 AM':'am11_30','11:30:00 PM':'pm11_30','11:45:00 AM':'am11_45','11:45:00 PM':'pm11_45','12:00:00 AM':'am0','12:00:00 PM':'pm12','12:15:00 AM':'am0_15','12:15:00 PM':'pm12_15','12:30:00 AM':'am0_30','12:30:00 PM':'pm12_30','12:45:00 AM':'am0_45','12:45:00 PM':'pm12_45'})


# In[ ]:


# df_rename.to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\NHD\processed\test.xlsx')


# In[ ]:


total_df = df_rename.groupby(['Date','Date_1','stationno','Location','Road','Mon','Year','Day','County','Agency','Interval','Counttype','Onetwoway']).sum().reset_index()
total_df['Direction'] = 'Total'
total_df['otherdirection'] = ''
concat_final = pd.concat([df_rename,total_df])


# In[ ]:


concat_final['PM_ID'] = ''
concat_final['Roadtype'] = 'Road'
concat_final['Otherlocation'] = ''
concat_final['comments'] = ''
concat_final['Location'] = concat_final.Location.replace({'south of':'s/o','west of':'w/o','north of':'n/o','east of':'e/o'},regex=True)
concat_final[[u'stationno', u'PM_ID', u'County', u'Agency',
       u'Interval', u'Counttype', u'Roadtype', u'Onetwoway', u'Day',
       u'Date', u'Mon', u'Year', u'Road', u'Location', u'Otherlocation',
       u'Direction', u'otherdirection', u'Total', u'am0', u'am0_15', u'am0_30', u'am0_45',
       u'am1', u'am1_15', u'am1_30', u'am1_45', u'am2', u'am2_15',
       u'am2_30', u'am2_45', u'am3', u'am3_15', u'am3_30', u'am3_45',
       u'am4', u'am4_15', u'am4_30', u'am4_45', u'am5', u'am5_15',
       u'am5_30', u'am5_45', u'am6', u'am6_15', u'am6_30', u'am6_45',
       u'am7', u'am7_15', u'am7_30', u'am7_45', u'am8', u'am8_15',
       u'am8_30', u'am8_45', u'am9', u'am9_15', u'am9_30', u'am9_45',
       u'am10', u'am10_15', u'am10_30', u'am10_45', u'am11', u'am11_15',
       u'am11_30', u'am11_45', u'pm12', u'pm12_15', u'pm12_30', u'pm12_45',
       u'pm1', u'pm1_15', u'pm1_30', u'pm1_45', u'pm2', u'pm2_15',
       u'pm2_30', u'pm2_45', u'pm3', u'pm3_15', u'pm3_30', u'pm3_45',
       u'pm4', u'pm4_15', u'pm4_30', u'pm4_45', u'pm5', u'pm5_15',
       u'pm5_30', u'pm5_45', u'pm6', u'pm6_15', u'pm6_30', u'pm6_45',
       u'pm7', u'pm7_15', u'pm7_30', u'pm7_45', u'pm8', u'pm8_15',
       u'pm8_30', u'pm8_45', u'pm9', u'pm9_15', u'pm9_30', u'pm9_45',
       u'pm10', u'pm10_15', u'pm10_30', u'pm10_45', u'pm11', u'pm11_15', u'pm11_30', u'pm11_45','comments']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\NHD\Processed\NHD_Processed_counts.xlsx',index=False)

