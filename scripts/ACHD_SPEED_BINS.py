
# coding: utf-8

# In[ ]:


import pandas as pd
import glob,os
from datetime import datetime


# In[ ]:


sixtyfifteen = raw_input('60 or 15')


# In[ ]:


pmid_ref = pd.read_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\PMID master look up for HM.xlsx')


# In[ ]:


df_list = []

if sixtyfifteen == '60':
    path = r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ACHD\batch\Speed60'
elif sixtyfifteen == '15':
    path = r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ACHD\batch\Speed15'
for sp_file in glob.glob(path +"/*.xls"):
    try:
        df = pd.ExcelFile(sp_file)
        sht_names = df.sheet_names
        for sht in sht_names:
            print sht
            df_cts= pd.read_excel(sp_file,sheetname=sht)
            if len(df_cts.columns.values[0]) > 2:
                print 'Missing Direction in '+ sp_file + " " + sht
                next(sp_file)
            else:
                df_cts1 = df_cts.iloc[10:,:]
                df_cts1.columns = df_cts1.iloc[0]
                df_cts2 = df_cts1.drop(df_cts1.index[0])
                df_cts2['Direction'] = df_cts.columns.values[0]
                df_cts2['Stationno'] = df_cts.loc[3].values[0].split(': ')[1]
                df_cts2['Road'] = df_cts.loc[5].values[0].split(': ')[1]
                df_cts2['Location'] = df_cts.loc[6].values[0].split(': ')[1]
                df_cts2['Long'] = (df_cts.iloc[7].values[0]).split(": ")[1]
                df_cts2['Lat'] = (df_cts.iloc[8].values[0]).split(": ")[1]
                df_cts2['Time'] = pd.to_datetime(df_cts2['Time']).dt.strftime('%I:%M:%S %p')
                df_cts2['Date1'] = pd.to_datetime(df_cts2['Date'])
                df_list.append(df_cts2)
    except:
        print "This record failed due to formatting "+sp_file+" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        continue
all_counts = pd.concat(df_list)


# In[ ]:


all_counts_melt = pd.melt(all_counts,['Road','Location','Date','Time','Date1','Direction','Lat','Long','Stationno'],var_name='Speed',value_name='Count')
all_counts_melt.dropna(axis=0,subset=['Count'],inplace=True)


# In[ ]:


all_counts_melt['Count'] = all_counts_melt['Count'].astype(int)
all_counts_piv = all_counts_melt.pivot_table(index=['Road','Location','Date','Date1','Direction','Lat','Long','Speed','Stationno'],columns='Time',values='Count',aggfunc='sum',margins=True,margins_name='Total').reset_index()
all_counts_rnm = all_counts_piv.rename(columns={'01:00:00 AM':'am1','01:00:00 PM':'pm1','01:15:00 AM':'am1_15','01:15:00 PM':'pm1_15','01:30:00 AM':'am1_30','01:30:00 PM':'pm1_30','01:45:00 AM':'am1_45','01:45:00 PM':'pm1_45','02:00:00 AM':'am2','02:00:00 PM':'pm2','02:15:00 AM':'am2_15','02:15:00 PM':'pm2_15','02:30:00 AM':'am2_30','02:30:00 PM':'pm2_30','02:45:00 AM':'am2_45','02:45:00 PM':'pm2_45','03:00:00 AM':'am3','03:00:00 PM':'pm3','03:15:00 AM':'am3_15','03:15:00 PM':'pm3_15','03:30:00 AM':'am3_30','03:30:00 PM':'pm3_30','03:45:00 AM':'am3_45','03:45:00 PM':'pm3_45','04:00:00 AM':'am4','04:00:00 PM':'pm4','04:15:00 AM':'am4_15','04:15:00 PM':'pm4_15','04:30:00 AM':'am4_30','04:30:00 PM':'pm4_30','04:45:00 AM':'am4_45','04:45:00 PM':'pm4_45','05:00:00 AM':'am5','05:00:00 PM':'pm5','05:15:00 AM':'am5_15','05:15:00 PM':'pm5_15','05:30:00 AM':'am5_30','05:30:00 PM':'pm5_30','05:45:00 AM':'am5_45','05:45:00 PM':'pm5_45','06:00:00 AM':'am6','06:00:00 PM':'pm6','06:15:00 AM':'am6_15','06:15:00 PM':'pm6_15','06:30:00 AM':'am6_30','06:30:00 PM':'pm6_30','06:45:00 AM':'am6_45','06:45:00 PM':'pm6_45','07:00:00 AM':'am7','07:00:00 PM':'pm7','07:15:00 AM':'am7_15','07:15:00 PM':'pm7_15','07:30:00 AM':'am7_30','07:30:00 PM':'pm7_30','07:45:00 AM':'am7_45','07:45:00 PM':'pm7_45','08:00:00 AM':'am8','08:00:00 PM':'pm8','08:15:00 AM':'am8_15','08:15:00 PM':'pm8_15','08:30:00 AM':'am8_30','08:30:00 PM':'pm8_30','08:45:00 AM':'am8_45','08:45:00 PM':'pm8_45','09:00:00 AM':'am9','09:00:00 PM':'pm9','09:15:00 AM':'am9_15','09:15:00 PM':'pm9_15','09:30:00 AM':'am9_30','09:30:00 PM':'pm9_30','09:45:00 AM':'am9_45','09:45:00 PM':'pm9_45','10:00:00 AM':'am10','10:00:00 PM':'pm10','10:15:00 AM':'am10_15','10:15:00 PM':'pm10_15','10:30:00 AM':'am10_30','10:30:00 PM':'pm10_30','10:45:00 AM':'am10_45','10:45:00 PM':'pm10_45','11:00:00 AM':'am11','11:00:00 PM':'pm11','11:15:00 AM':'am11_15','11:15:00 PM':'pm11_15','11:30:00 AM':'am11_30','11:30:00 PM':'pm11_30','11:45:00 AM':'am11_45','11:45:00 PM':'pm11_45','12:00:00 AM':'am0','12:00:00 PM':'pm12','12:15:00 AM':'am0_15','12:15:00 PM':'pm12_15','12:30:00 AM':'am0_30','12:30:00 PM':'pm12_30','12:45:00 AM':'am0_45','12:45:00 PM':'pm12_45'})


# In[ ]:


def direction(x):
    if x == 'NB':
        return 'North'
    if x == 'SB':
        return 'South'
    if x == 'EB':
        return 'East'
    if x == 'WB':
        return 'West'
    else:
        return x
all_counts_rnm['Direction'] = all_counts_rnm['Direction'].apply(lambda x: direction(x))

all_counts_rnm['Location'] = all_counts_rnm.Location.replace({'N/O':'n/o','S/O':'s/o','E/O':'e/o','W/O':'w/o','N-O':'n/o','S-O':'s/o','W-O':'w/o','E-O':'e/o'},regex=True)

all_counts_rnm['Agency'] = 'ACHD'
all_counts_rnm['Segmentcode'] = ''
all_counts_rnm['County'] = 'Ada'
all_counts_rnm['onetwoway'] = '2'
all_counts_rnm['Counttype'] = 'Speed'
if sixtyfifteen == '60':
    all_counts_rnm['Interval'] = '60'
elif sixtyfifteen == '15':
    all_counts_rnm['Interval'] = '15'
all_counts_rnm['Roadtype'] ='Road'
all_counts_rnm['Mon'] = pd.to_datetime(all_counts_rnm['Date1']).dt.strftime('%b')
all_counts_rnm['Year'] = all_counts_rnm['Date1'].dt.year
all_counts_rnm['Day'] = pd.to_datetime(all_counts_rnm['Date1']).dt.strftime('%a')

def add_zero(x):
    if x[0] == '0':
        return x
    elif x[1].isdigit():
        return x
    elif x[0].isalpha() :
        return x
    else:
        return '0'+x
        
all_counts_rnm['Road'] = all_counts_rnm['Road'].apply(lambda x: add_zero(x))


# In[ ]:



totaltotal = all_counts_rnm.groupby(['Road','Location','Date','Date1','Lat','Long','Stationno','Agency','Segmentcode','County','onetwoway','Counttype','Interval','Roadtype','Mon','Year','Day']).sum()
totaltotal['Direction'] = 'Total'
totaltotal['Speed'] = 'Total'
totaltotal.reset_index(inplace=True)


totaldir = all_counts_rnm.groupby(['Road','Location','Direction','Date','Date1','Lat','Long','Stationno','Agency','Segmentcode','County','onetwoway','Counttype','Interval','Roadtype','Mon','Year','Day']).sum()
totaldir.reset_index(inplace = True)
totaldir.loc[:,'Direction']= totaldir.loc[:,'Direction'].apply(lambda x: 'Total '+ x)
totaldir.loc[:,'Speed'] = totaldir.loc[:,'Direction']

df_final_mrg = pd.concat([all_counts_rnm,totaltotal,totaldir])


# In[ ]:


df_final = df_final_mrg.merge(pmid_ref,how='left',left_on=['Road','Location','County'],right_on=['road','location','county'])
df_final['PM_ID'] = df_final['pm_id']


# In[ ]:


if sixtyfifteen == '60':
    df_final[[u'PM_ID',u'Stationno',u'Segmentcode',u'Agency',u'County',u'onetwoway',u'Counttype',u'Interval',u'Roadtype',
     u'Mon',u'Year',u'Date',u'Day',u'Road',u'Location',u'Direction',u'Speed',u'Total',u'am0',u'am1',u'am2',u'am3',u'am4',
     u'am5',u'am6',u'am7',u'am8',u'am9',u'am10',u'am11',u'pm12',u'pm1',u'pm2',u'pm3',u'pm4',u'pm5',u'pm6',u'pm7',u'pm8',u'pm9',
     u'pm10',u'pm11','Lat','Long']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ACHD\batch\Speed60Processed\speed_60_final.xlsx',index=False)
    
elif sixtyfifteen == '15':
        df_final[[u'PM_ID',u'Stationno',u'Segmentcode',u'Agency',u'County',u'onetwoway',u'Counttype',u'Interval',u'Roadtype',
     u'Mon',u'Year',u'Date',u'Day',u'Road',u'Location',u'Direction',u'Speed',u'Total',u'am0',u'am0_15',u'am0_30',u'am0_45',u'am1',u'am1_15',u'am1_30',u'am1_45',u'am2',u'am2_15', u'am2_30',u'am2_45',u'am3',u'am3_15',u'am3_30',u'am3_45',u'am4',u'am4_15',u'am4_30',u'am4_45',u'am5',u'am5_15',u'am5_30', u'am5_45',u'am6',u'am6_15',u'am6_30',u'am6_45',u'am7',u'am7_15',u'am7_30',u'am7_45',u'am8',u'am8_15', u'am8_30',u'am8_45',u'am9',u'am9_15',u'am9_30',u'am9_45',u'am10',u'am10_15',u'am10_30',u'am10_45', u'am11',u'am11_15',u'am11_30',u'am11_45',u'pm12',u'pm12_15',u'pm12_30',u'pm12_45',u'pm1',u'pm1_15',u'pm1_30', u'pm1_45',u'pm2',u'pm2_15',u'pm2_30',u'pm2_45',u'pm3',u'pm3_15',u'pm3_30',u'pm3_45',u'pm4',u'pm4_15', u'pm4_30',u'pm4_45',u'pm5',u'pm5_15',u'pm5_30',u'pm5_45',u'pm6',u'pm6_15',u'pm6_30',u'pm6_45',u'pm7', u'pm7_15',u'pm7_30',u'pm7_45',u'pm8',u'pm8_15',u'pm8_30',u'pm8_45',u'pm9',u'pm9_15',u'pm9_30',u'pm9_45', u'pm10',u'pm10_15',u'pm10_30',u'pm10_45',u'pm11',u'pm11_15',u'pm11_30',u'pm11_45','Lat','Long']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ACHD\batch\Speed15Processed\speed_15_final.xlsx',index=False)
    

