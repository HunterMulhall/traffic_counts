
# coding: utf-8

# In[1]:


#import modules
import pandas as pd
import glob,os
from datetime import datetime


# In[2]:


fifteen_sixty = raw_input('15 or 60')


# In[3]:


#Load PM_ID lookup table
pmid_ref = pd.read_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\PMID master look up for HM_v2.xlsx')


# In[4]:


#iterate through the count excel files and Grab relevant data.  Merge all the counts into one dataframe
df_list = []

if fifteen_sixty == '15':
    path = r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\Nampa\15min\One_Way'
elif fifteen_sixty == '60':
    path = r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\Nampa\60min\One_Way'

for count in glob.glob(path +"/*.xls"):
    df = pd.read_excel(count,skiprows=11)
    df['Direction'] = df.columns.values[2]
    df2 = pd.read_excel(count).iloc[:8,:1]
    df['stationno'] = df2.iloc[2].values[0].split(" ")[2]
    df['Road'] = (df2.iloc[4].values[0]).split(": ")[1]
    df['Location'] = (df2.iloc[5].values[0]).split(": ")[1]
    df['Long'] = (df2.iloc[6].values[0]).split(": ")[1]
    df['Lat'] = (df2.iloc[7].values[0]).split(": ")[1]
    df['Time'] = pd.to_datetime(df['Time']).dt.strftime('%I:%M:%S %p')
    df['Date1'] = pd.to_datetime(df['Date'])
    df.rename(columns={'NB':'Counts','SB':'Counts','EB':'Counts','WB':'Counts'},inplace=True)
    df_list.append(df)
all_counts = pd.concat(df_list)


# In[5]:


#Pivot the concatonated Dataframe
df_piv = all_counts.pivot_table(index=['Date','Date1','Direction','stationno','Road','Location','Long','Lat'],columns='Time',values='Counts',aggfunc='sum',margins=True,margins_name='Total').reset_index()


# In[6]:


#Rename the column header to match schema.  Add fields to match schema.  Reformat fields to match schema.

df_piv_rnm = df_piv.rename(columns={'01:00:00 AM':'am1','01:00:00 PM':'pm1','01:15:00 AM':'am1_15','01:15:00 PM':'pm1_15','01:30:00 AM':'am1_30','01:30:00 PM':'pm1_30','01:45:00 AM':'am1_45','01:45:00 PM':'pm1_45','02:00:00 AM':'am2','02:00:00 PM':'pm2','02:15:00 AM':'am2_15','02:15:00 PM':'pm2_15','02:30:00 AM':'am2_30','02:30:00 PM':'pm2_30','02:45:00 AM':'am2_45','02:45:00 PM':'pm2_45','03:00:00 AM':'am3','03:00:00 PM':'pm3','03:15:00 AM':'am3_15','03:15:00 PM':'pm3_15','03:30:00 AM':'am3_30','03:30:00 PM':'pm3_30','03:45:00 AM':'am3_45','03:45:00 PM':'pm3_45','04:00:00 AM':'am4','04:00:00 PM':'pm4','04:15:00 AM':'am4_15','04:15:00 PM':'pm4_15','04:30:00 AM':'am4_30','04:30:00 PM':'pm4_30','04:45:00 AM':'am4_45','04:45:00 PM':'pm4_45','05:00:00 AM':'am5','05:00:00 PM':'pm5','05:15:00 AM':'am5_15','05:15:00 PM':'pm5_15','05:30:00 AM':'am5_30','05:30:00 PM':'pm5_30','05:45:00 AM':'am5_45','05:45:00 PM':'pm5_45','06:00:00 AM':'am6','06:00:00 PM':'pm6','06:15:00 AM':'am6_15','06:15:00 PM':'pm6_15','06:30:00 AM':'am6_30','06:30:00 PM':'pm6_30','06:45:00 AM':'am6_45','06:45:00 PM':'pm6_45','07:00:00 AM':'am7','07:00:00 PM':'pm7','07:15:00 AM':'am7_15','07:15:00 PM':'pm7_15','07:30:00 AM':'am7_30','07:30:00 PM':'pm7_30','07:45:00 AM':'am7_45','07:45:00 PM':'pm7_45','08:00:00 AM':'am8','08:00:00 PM':'pm8','08:15:00 AM':'am8_15','08:15:00 PM':'pm8_15','08:30:00 AM':'am8_30','08:30:00 PM':'pm8_30','08:45:00 AM':'am8_45','08:45:00 PM':'pm8_45','09:00:00 AM':'am9','09:00:00 PM':'pm9','09:15:00 AM':'am9_15','09:15:00 PM':'pm9_15','09:30:00 AM':'am9_30','09:30:00 PM':'pm9_30','09:45:00 AM':'am9_45','09:45:00 PM':'pm9_45','10:00:00 AM':'am10','10:00:00 PM':'pm10','10:15:00 AM':'am10_15','10:15:00 PM':'pm10_15','10:30:00 AM':'am10_30','10:30:00 PM':'pm10_30','10:45:00 AM':'am10_45','10:45:00 PM':'pm10_45','11:00:00 AM':'am11','11:00:00 PM':'pm11','11:15:00 AM':'am11_15','11:15:00 PM':'pm11_15','11:30:00 AM':'am11_30','11:30:00 PM':'pm11_30','11:45:00 AM':'am11_45','11:45:00 PM':'pm11_45','12:00:00 AM':'am0','12:00:00 PM':'pm12','12:15:00 AM':'am0_15','12:15:00 PM':'pm12_15','12:30:00 AM':'am0_30','12:30:00 PM':'pm12_30','12:45:00 AM':'am0_45','12:45:00 PM':'pm12_45'})
df_piv_rnm['PM_ID'] = ''
df_piv_rnm['County'] = 'Canyon'
df_piv_rnm['Agency'] = 'Nampa'

if fifteen_sixty == '60':
    df_piv_rnm['Interval'] = '60'
elif fifteen_sixty == '15':
    df_piv_rnm['Interval'] = '15'
df_piv_rnm['Counttype'] = 'Portable'
df_piv_rnm['Roadtype'] = 'Road'
df_piv_rnm['Onetwoway'] = '1'
df_piv_rnm['Day'] = pd.to_datetime(df_piv_rnm['Date1']).dt.strftime('%a')
df_piv_rnm['Mon'] = pd.to_datetime(df_piv_rnm['Date1']).dt.strftime('%b')
df_piv_rnm['Year'] = df_piv_rnm['Date1'].dt.year
df_piv_rnm['Otherlocation'] = ''
df_piv_rnm['otherdirection'] =''
df_piv_rnm['comments'] =''

def direction(x):
    if x == 'NB':
        return 'North'
    if x == 'SB':
        return 'South'
    if x == 'EB':
        return 'East'
    if x == 'WB':
        return 'West'
    else:
        return x
df_piv_rnm['Direction'] = df_piv_rnm['Direction'].apply(lambda x: direction(x))


# In[7]:


#Reformat fields to Match Schema.  Make sure street names have a zero in front so PM_ID lookup matches.

df_piv_rnm['Location'] = df_piv_rnm.Location.replace({'N/O':'n/o','S/O':'s/o','E/O':'e/o','W/O':'w/o','N-O':'n/o','S-O':'s/o','W-O':'w/o','E-O':'e/o'},regex=True)
df_piv_rnm.loc[df_piv_rnm.Date == 'Total','Road'] = 'Total'

def add_zero(x):
    if x[0] == '0':
        return x
    elif x[1].isdigit():
        return x
    elif x[0].isalpha() :
        return x
    else:
        return '0'+x
        
df_piv_rnm['Road'] = df_piv_rnm['Road'].apply(lambda x: add_zero(x))


# In[8]:


#merge pm_id lookup values and populate PM_ID field
df_final = df_piv_rnm.merge(pmid_ref,how='left',left_on=['Road','Location','County'],right_on=['road','location','county'])
df_final['PM_ID'] = df_final['pm_id']


# In[9]:


#output final dataframe to excel
if fifteen_sixty == '60':
    df_final[[u'stationno', u'PM_ID', u'County', u'Agency',
           u'Interval', u'Counttype', u'Roadtype', u'Onetwoway', u'Day',
           u'Date', u'Mon', u'Year', u'Road', u'Location', u'Otherlocation',
           u'Direction', u'otherdirection', u'Total', u'am0',
           u'am1', u'am2', u'am3',u'am4', u'am5', u'am6', u'am7', u'am8', u'am9', u'am10', u'am11',u'pm12',
           u'pm1', u'pm2', u'pm3', u'pm4', u'pm5', u'pm6',
           u'pm7', u'pm8', u'pm9', u'pm10', u'pm11','comments','Lat','Long']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\Nampa\60min\One_Way\oneway60_final.xlsx',index=False)
elif fifteen_sixty == '15':
        df_final[[u'stationno', u'PM_ID', u'County', u'Agency',
           u'Interval', u'Counttype', u'Roadtype', u'Onetwoway', u'Day',
           u'Date', u'Mon', u'Year', u'Road', u'Location', u'Otherlocation',
           u'Direction', u'otherdirection', u'Total', u'am0',u'am0_15',u'am0_30',u'am0_45',u'am1',u'am1_15',u'am1_30',u'am1_45',u'am2',u'am2_15', u'am2_30',u'am2_45',u'am3',u'am3_15',u'am3_30',u'am3_45',u'am4',u'am4_15',u'am4_30',u'am4_45',u'am5',u'am5_15',u'am5_30', u'am5_45',u'am6',u'am6_15',u'am6_30',u'am6_45',u'am7',u'am7_15',u'am7_30',u'am7_45',u'am8',u'am8_15', u'am8_30',u'am8_45',u'am9',u'am9_15',u'am9_30',u'am9_45',u'am10',u'am10_15',u'am10_30',u'am10_45', u'am11',u'am11_15',u'am11_30',u'am11_45',u'pm12',u'pm12_15',u'pm12_30',u'pm12_45',u'pm1',u'pm1_15',u'pm1_30', u'pm1_45',u'pm2',u'pm2_15',u'pm2_30',u'pm2_45',u'pm3',u'pm3_15',u'pm3_30',u'pm3_45',u'pm4',u'pm4_15', u'pm4_30',u'pm4_45',u'pm5',u'pm5_15',u'pm5_30',u'pm5_45',u'pm6',u'pm6_15',u'pm6_30',u'pm6_45',u'pm7', u'pm7_15',u'pm7_30',u'pm7_45',u'pm8',u'pm8_15',u'pm8_30',u'pm8_45',u'pm9',u'pm9_15',u'pm9_30',u'pm9_45', u'pm10',u'pm10_15',u'pm10_30',u'pm10_45',u'pm11',u'pm11_15',u'pm11_30',u'pm11_45','comments','Lat','Long']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\Nampa\15min\One_Way\oneway15_final.xlsx',index=False)

