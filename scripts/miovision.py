
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
from datetime import datetime,timedelta, date
import glob,os


# In[ ]:


path = raw_input('What is the path to the directory with the count files?')
os.chdir(path)
records = glob.glob("*.prn")
df_out = pd.DataFrame()
lookup = pd.read_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ITD\miovision\PMIDlookup_nonI84.xlsx')


# In[ ]:


for record in records:
    try:
        print record
        df2 = pd.read_table(record,sep=" ",skiprows=6,header=None,dtype={3: np.object})
        df3 = pd.read_table(record,nrows=5,names=['CODE'])
        if df3.iloc[3]['CODE'].split(" ")[0] != '01':
            print 'NOT a Volume Count: '+record+'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            next(record)
        df2['Date'] = 0
        df2['Direction'] = np.nan
        df2.loc[df2[2]==0,'Direction'] = 'North'
        df2.loc[df2[2]==1,'Direction'] = 'East'
        df2.loc[df2[2]==2,'Direction'] = 'South'
        df2.loc[df2[2]==3,'Direction'] = 'West'
        df2['Count'] = df2[4]
        date_time_collection = df3.iloc[0]['CODE']
        start_time = datetime.strptime(date_time_collection.split(' ')[4]+' '+date_time_collection.split(' ')[3],'%m%d%y %H%M')
        add_15 = timedelta(minutes=15)
        start_15 = start_time+add_15
        end_time = datetime.strptime(date_time_collection.split(' ')[6]+' '+date_time_collection.split(' ')[5],'%m%d%y %H%M')
        print start_time
        print start_15
        print end_time
        df2.loc[df2[1]==1,'Date'] = pd.date_range(start=start_15,end=end_time,freq='15T')
        df2.loc[df2[1]==2,'Date'] = pd.date_range(start=start_15,end=end_time,freq='15T')
        df2['Location']= record
        df2['Direction']
        print 'done'
        df_out = df_out.append(df2,ignore_index=True)
    except:
        print "This record failed due to formatting "+record+" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        continue


# In[ ]:


# df_out[['Location','Date','Direction','Count']].to_csv(r"T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ITD\miovision\LegVolumeData\processedmiovision_counts.csv")


# In[ ]:


df_out['Date_1']= df_out['Date'].apply(lambda x: x.date())
df_out['Time']=df_out['Date'].apply(lambda x: x.strftime('%I:%M:%S %p'))


# In[ ]:


piv_df = df_out.pivot_table(index=['Location','Date_1','Direction'],columns='Time',values='Count',aggfunc= np.sum,margins=True,margins_name='Total')
piv_df.reset_index(inplace=True)
piv_df['Mon'] = piv_df.Date_1.dt.strftime('%b')
piv_df['Year'] = piv_df.Date_1.dt.year
piv_df['Counttype'] = 'miovision'
piv_df['Interval'] = '15'
piv_df['County'] = ''
piv_df['Agency'] = 'ITD'
piv_df['Roadtype'] = 'Road'
piv_df['Onetwoway'] = ''
df_rename = piv_df.rename(columns={'01:00:00 AM':'am1','01:00:00 PM':'pm1','01:15:00 AM':'am1_15','01:15:00 PM':'pm1_15','01:30:00 AM':'am1_30','01:30:00 PM':'pm1_30','01:45:00 AM':'am1_45','01:45:00 PM':'pm1_45','02:00:00 AM':'am2','02:00:00 PM':'pm2','02:15:00 AM':'am2_15','02:15:00 PM':'pm2_15','02:30:00 AM':'am2_30','02:30:00 PM':'pm2_30','02:45:00 AM':'am2_45','02:45:00 PM':'pm2_45','03:00:00 AM':'am3','03:00:00 PM':'pm3','03:15:00 AM':'am3_15','03:15:00 PM':'pm3_15','03:30:00 AM':'am3_30','03:30:00 PM':'pm3_30','03:45:00 AM':'am3_45','03:45:00 PM':'pm3_45','04:00:00 AM':'am4','04:00:00 PM':'pm4','04:15:00 AM':'am4_15','04:15:00 PM':'pm4_15','04:30:00 AM':'am4_30','04:30:00 PM':'pm4_30','04:45:00 AM':'am4_45','04:45:00 PM':'pm4_45','05:00:00 AM':'am5','05:00:00 PM':'pm5','05:15:00 AM':'am5_15','05:15:00 PM':'pm5_15','05:30:00 AM':'am5_30','05:30:00 PM':'pm5_30','05:45:00 AM':'am5_45','05:45:00 PM':'pm5_45','06:00:00 AM':'am6','06:00:00 PM':'pm6','06:15:00 AM':'am6_15','06:15:00 PM':'pm6_15','06:30:00 AM':'am6_30','06:30:00 PM':'pm6_30','06:45:00 AM':'am6_45','06:45:00 PM':'pm6_45','07:00:00 AM':'am7','07:00:00 PM':'pm7','07:15:00 AM':'am7_15','07:15:00 PM':'pm7_15','07:30:00 AM':'am7_30','07:30:00 PM':'pm7_30','07:45:00 AM':'am7_45','07:45:00 PM':'pm7_45','08:00:00 AM':'am8','08:00:00 PM':'pm8','08:15:00 AM':'am8_15','08:15:00 PM':'pm8_15','08:30:00 AM':'am8_30','08:30:00 PM':'pm8_30','08:45:00 AM':'am8_45','08:45:00 PM':'pm8_45','09:00:00 AM':'am9','09:00:00 PM':'pm9','09:15:00 AM':'am9_15','09:15:00 PM':'pm9_15','09:30:00 AM':'am9_30','09:30:00 PM':'pm9_30','09:45:00 AM':'am9_45','09:45:00 PM':'pm9_45','10:00:00 AM':'am10','10:00:00 PM':'pm10','10:15:00 AM':'am10_15','10:15:00 PM':'pm10_15','10:30:00 AM':'am10_30','10:30:00 PM':'pm10_30','10:45:00 AM':'am10_45','10:45:00 PM':'pm10_45','11:00:00 AM':'am11','11:00:00 PM':'pm11','11:15:00 AM':'am11_15','11:15:00 PM':'pm11_15','11:30:00 AM':'am11_30','11:30:00 PM':'pm11_30','11:45:00 AM':'am11_45','11:45:00 PM':'pm11_45','12:00:00 AM':'am0','12:00:00 PM':'pm12','12:15:00 AM':'am0_15','12:15:00 PM':'pm12_15','12:30:00 AM':'am0_30','12:30:00 PM':'pm12_30','12:45:00 AM':'am0_45','12:45:00 PM':'pm12_45'})


# In[ ]:


df_total = df_rename.groupby(['Location','Date_1','Mon','Year','Counttype','Interval','County','Agency','Roadtype','Onetwoway']).sum().reset_index()
df_total['Direction'] = 'Total'
df_concat = pd.concat([df_rename,df_total])
df_concat['Day'] = df_concat['Date_1'].dt.strftime('%a')
df_concat['Date'] = df_concat['Date_1'].dt.strftime('%m/%d/%Y')
df_concat['Road'] = ''
df_concat['stationno'] = ''
df_concat['PM_ID'] = ''
df_concat['comments'] = ''
df_concat['Otherlocation'] = ''
df_concat['otherdirection'] = ''


# In[ ]:


df_concat[[u'stationno', u'PM_ID', u'County', u'Agency',
       u'Interval', u'Counttype', u'Roadtype', u'Onetwoway', u'Day',
       u'Date', u'Mon', u'Year', u'Road', u'Location', u'Otherlocation',
       u'Direction', u'otherdirection', u'Total', u'am0', u'am0_15', u'am0_30', u'am0_45',
       u'am1', u'am1_15', u'am1_30', u'am1_45', u'am2', u'am2_15',
       u'am2_30', u'am2_45', u'am3', u'am3_15', u'am3_30', u'am3_45',
       u'am4', u'am4_15', u'am4_30', u'am4_45', u'am5', u'am5_15',
       u'am5_30', u'am5_45', u'am6', u'am6_15', u'am6_30', u'am6_45',
       u'am7', u'am7_15', u'am7_30', u'am7_45', u'am8', u'am8_15',
       u'am8_30', u'am8_45', u'am9', u'am9_15', u'am9_30', u'am9_45',
       u'am10', u'am10_15', u'am10_30', u'am10_45', u'am11', u'am11_15',
       u'am11_30', u'am11_45', u'pm12', u'pm12_15', u'pm12_30', u'pm12_45',
       u'pm1', u'pm1_15', u'pm1_30', u'pm1_45', u'pm2', u'pm2_15',
       u'pm2_30', u'pm2_45', u'pm3', u'pm3_15', u'pm3_30', u'pm3_45',
       u'pm4', u'pm4_15', u'pm4_30', u'pm4_45', u'pm5', u'pm5_15',
       u'pm5_30', u'pm5_45', u'pm6', u'pm6_15', u'pm6_30', u'pm6_45',
       u'pm7', u'pm7_15', u'pm7_30', u'pm7_45', u'pm8', u'pm8_15',
       u'pm8_30', u'pm8_45', u'pm9', u'pm9_15', u'pm9_30', u'pm9_45',
       u'pm10', u'pm10_15', u'pm10_30', u'pm10_45', u'pm11', u'pm11_15', u'pm11_30', u'pm11_45','comments']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ITD\miovision\LegVolumeData\processed_miovision.xlsx',index=False)


# In[ ]:


#postprocessing for Mary Ann!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
df_44 = pd.read_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ITD\miovision\LegVolumeData\SH44Mio_MiddletonMW.xlsx')
df_44_mrg = df_44.merge(lookup, how ='left',left_on = ['Road Name','Location'],right_on = ['road','location'])
df_44_mrg['Mon'] = df_44_mrg.Date_1.dt.strftime('%b')
df_44_mrg['Year'] = df_44_mrg.Date_1.dt.year
df_44_mrg['Counttype'] = 'miovision'
df_44_mrg['Interval'] = '15'
df_44_mrg['County'] = ''
df_44_mrg['Agency'] = 'ITD'
df_44_mrg['Roadtype'] = 'Road'
df_44_mrg['Onetwoway'] = ''
df_44_mrg


# In[ ]:


import datetime
df_44_mrg[['pm_id','County','Agency','Interval','Counttype','Roadtype','Onetwoway','Date_1','Mon','Year','Road Name','road','Type','Location','location','Location of MioVision Counter','Direction','Total', datetime.time(0, 0),
       datetime.time(0, 15), datetime.time(0, 30), datetime.time(0, 45),
       datetime.time(1, 0), datetime.time(1, 15), datetime.time(1, 30),
       datetime.time(1, 45), datetime.time(2, 0), datetime.time(2, 15),
       datetime.time(2, 30), datetime.time(2, 45), datetime.time(3, 0),
       datetime.time(3, 15), datetime.time(3, 30), datetime.time(3, 45),
       datetime.time(4, 0), datetime.time(4, 15), datetime.time(4, 30),
       datetime.time(4, 45), datetime.time(5, 0), datetime.time(5, 15),
       datetime.time(5, 30), datetime.time(5, 45), datetime.time(6, 0),
       datetime.time(6, 15), datetime.time(6, 30), datetime.time(6, 45),
       datetime.time(7, 0), datetime.time(7, 15), datetime.time(7, 30),
       datetime.time(7, 45), datetime.time(8, 0), datetime.time(8, 15),
       datetime.time(8, 30), datetime.time(8, 45), datetime.time(9, 0),
       datetime.time(9, 15), datetime.time(9, 30), datetime.time(9, 45),
       datetime.time(10, 0), datetime.time(10, 15), datetime.time(10, 30),
       datetime.time(10, 45), datetime.time(11, 0), datetime.time(11, 15),
       datetime.time(11, 30), datetime.time(11, 45), datetime.time(12, 0),
       datetime.time(12, 15), datetime.time(12, 30), datetime.time(12, 45),
       datetime.time(13, 0), datetime.time(13, 15), datetime.time(13, 30),
       datetime.time(13, 45), datetime.time(14, 0), datetime.time(14, 15),
       datetime.time(14, 30), datetime.time(14, 45), datetime.time(15, 0),
       datetime.time(15, 15), datetime.time(15, 30), datetime.time(15, 45),
       datetime.time(16, 0), datetime.time(16, 15), datetime.time(16, 30),
       datetime.time(16, 45), datetime.time(17, 0), datetime.time(17, 15),
       datetime.time(17, 30), datetime.time(17, 45), datetime.time(18, 0),
       datetime.time(18, 15), datetime.time(18, 30), datetime.time(18, 45),
       datetime.time(19, 0), datetime.time(19, 15), datetime.time(19, 30),
       datetime.time(19, 45), datetime.time(20, 0), datetime.time(20, 15),
       datetime.time(20, 30), datetime.time(20, 45), datetime.time(21, 0),
       datetime.time(21, 15), datetime.time(21, 30), datetime.time(21, 45),
       datetime.time(22, 0), datetime.time(22, 15), datetime.time(22, 30),
       datetime.time(22, 45), datetime.time(23, 0), datetime.time(23, 15),
       datetime.time(23, 30), datetime.time(23, 45)]].to_csv(r"T:\Permanent\Resources\Traffic Counts\New Counts For Processing\ITD\miovision\LegVolumeData\processedmiovision_counts_piv_pmid.csv",index=False)

