
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
import glob
import os
from time import strptime
from datetime import datetime


# In[ ]:


df_list = []

path = raw_input('What is the path to the folder with counts for processing?')
for count in glob.glob(path +"/*.xls"):
    print count
    lar = pd.read_excel(count)
    test = lar.iloc[2,0]
    print test.split(": ")[1]
    lar.columns = lar.iloc[7]
    newdf = lar.iloc[8:]
    newdf['stationno'] = test.split(": ")[1]
    df_list.append(newdf)
all_counts = pd.concat(df_list)


# In[ ]:


all_counts['Direction 1'] = all_counts['Direction 1'].astype('int')
all_counts['Direction 2'] = all_counts['Direction 2'].astype('int')
all_counts['Time']= pd.to_datetime(all_counts['Time']).dt.strftime('%I:%M:%S %p')
all_counts_piv1 = all_counts.pivot_table(index=['Date','stationno'],columns=['Time'],values=['Direction 1']).reset_index()
all_counts_piv1.columns
all_counts_piv1['direction'] = 'South or West'
all_counts_piv1['direction_other'] = '1'
all_counts_piv1.columns = all_counts_piv1.columns.droplevel()
all_counts_piv1.columns.values[0] = 'Date'
all_counts_piv1.columns.values[1] = 'stationno'
all_counts_piv1.columns.values[98] = 'direction'
all_counts_piv1.columns.values[99] = 'direction_other'
all_counts_piv2 = all_counts.pivot_table(index=['Date','stationno'],columns=['Time'],values=['Direction 2']).reset_index()
all_counts_piv2['direction'] = 'North or East'
all_counts_piv2['direction_other'] = '2'
all_counts_piv2.columns = all_counts_piv2.columns.droplevel()
all_counts_piv2.columns.values[0] = 'Date'
all_counts_piv2.columns.values[1] = 'stationno'
all_counts_piv2.columns.values[98] = 'direction'
all_counts_piv2.columns.values[99] = 'direction_other'
all_counts_piv2
concat_counts = pd.concat([all_counts_piv1,all_counts_piv2])
# concat_counts.to_csv(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\CHD\excel2016_17\test.csv')


# In[ ]:


lookup = pd.read_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\CHD\Road Segment 2017.xlsx')


# In[ ]:


counts_mrg = lookup.merge(concat_counts,left_on='RoadNumber', right_on ='stationno')
counts_mrg['Date'] = pd.to_datetime(counts_mrg['Date'],format='%m/%d/%Y')
counts_mrg['Mon'] = counts_mrg['Date'].apply(lambda x: x.strftime('%B'))
counts_mrg['Year'] = counts_mrg['Date'].apply(lambda x: x.strftime('%Y'))
counts_mrg['day'] = counts_mrg['Date'].apply(lambda x: x.strftime('%A'))
counts_mrg['County'] = 'Canyon'
counts_mrg['Agency'] = 'CHD'
counts_mrg['Interval'] = '15'
counts_mrg['counttype'] = 'portable'
counts_mrg['onetwoway'] = '2'
counts_mrg['totalcount'] = counts_mrg.iloc[:,11:108].sum(axis=1)


# In[ ]:


hours_headers = counts_mrg.iloc[:,10:106].columns.tolist()
final_hours_headers = [u'am1',u'pm1',u'am1_15',u'pm1_15',u'am1_30',u'pm1_30',u'am1_45',u'pm1_45',u'am2',u'pm2',u'am2_15',u'pm2_15',
                       u'am2_30',u'pm2_30',u'am2_45',u'pm2_45',u'am3',u'pm3',u'am3_15',u'pm3_15',u'am3_30',u'pm3_30',u'am3_45',u'pm3_45',u'am4',u'pm4',u'am4_15',u'pm4_15',u'am4_30',u'pm4_30',u'am4_45',u'pm4_45',u'am5',u'pm5',u'am5_15',u'pm5_15',u'am5_30',u'pm5_30',
                       u'am5_45',u'pm5_45',u'am6',u'pm6',u'am6_15',u'pm6_15',u'am6_30',u'pm6_30',u'am6_45',u'pm6_45',u'am7',u'pm7',u'am7_15',u'pm7_15',u'am7_30',u'pm7_30',u'am7_45',u'pm7_45',u'am8',u'pm8',u'am8_15',u'pm8_15',
                       u'am8_30',u'pm8_30',u'am8_45',u'pm8_45',u'am9',u'pm9',u'am9_15',u'pm9_15',u'am9_30',u'pm9_30',u'am9_45',u'pm9_45',u'am10',u'pm10',u'am10_15',u'pm10_15',u'am10_30',u'pm10_30',u'am10_45',u'pm10_45',
                       u'am11',u'pm11',u'am11_15',u'pm11_15',u'am11_30',u'pm11_30',u'am11_45',u'pm11_45',u'am0',u'pm12',u'am0_15',u'pm12_15',u'am0_30',u'pm12_30',u'am0_45',u'pm12_45']
header_dict = dict(zip(hours_headers,final_hours_headers))
counts_mrg_rn= counts_mrg.rename(columns=header_dict)
counts_mrg_rn.rename(columns={'RoadName':'road','Location':'location','RoadType':'roadtype'},inplace=True)
counts_mrg_rn['pm_id'] = ''


# In[ ]:


counts_mrg_rn['Date'] = counts_mrg_rn['Date'].astype('str')


# In[ ]:


counts_total = counts_mrg_rn.groupby(['Date','Mon','Year','day','County','stationno','Agency','Interval','counttype','onetwoway','pm_id','road','location','roadtype']).sum().reset_index()
counts_total['direction'] = 'total'
counts_final = pd.concat([counts_mrg_rn,counts_total])
counts_final


# In[ ]:


counts_final[['Date','Mon','Year','day','County','stationno','Agency','Interval','counttype','onetwoway','pm_id','road','location','roadtype','direction','direction_other','totalcount',u'am0',u'am0_15',u'am0_30',u'am0_45',u'am1',u'am1_15',u'am1_30',u'am1_45',u'am2',u'am2_15',
                       u'am2_30',u'am2_45',u'am3',u'am3_15',u'am3_30',u'am3_45',u'am4',u'am4_15',u'am4_30',u'am4_45',u'am5',u'am5_15',u'am5_30',
                       u'am5_45',u'am6',u'am6_15',u'am6_30',u'am6_45',u'am7',u'am7_15',u'am7_30',u'am7_45',u'am8',u'am8_15',
                       u'am8_30',u'am8_45',u'am9',u'am9_15',u'am9_30',u'am9_45',u'am10',u'am10_15',u'am10_30',u'am10_45',
                       u'am11',u'am11_15',u'am11_30',u'am11_45',u'pm12',u'pm12_15',u'pm12_30',u'pm12_45',u'pm1',u'pm1_15',u'pm1_30',
                       u'pm1_45',u'pm2',u'pm2_15',u'pm2_30',u'pm2_45',u'pm3',u'pm3_15',u'pm3_30',u'pm3_45',u'pm4',u'pm4_15',
                       u'pm4_30',u'pm4_45',u'pm5',u'pm5_15',u'pm5_30',u'pm5_45',u'pm6',u'pm6_15',u'pm6_30',u'pm6_45',u'pm7',
                       u'pm7_15',u'pm7_30',u'pm7_45',u'pm8',u'pm8_15',u'pm8_30',u'pm8_45',u'pm9',u'pm9_15',u'pm9_30',u'pm9_45',
                       u'pm10',u'pm10_15',u'pm10_30',u'pm10_45',u'pm11',u'pm11_15',u'pm11_30',u'pm11_45']].to_excel(r'T:\Permanent\Resources\Traffic Counts\New Counts For Processing\CHD\processed_counts.xlsx',index=False)

